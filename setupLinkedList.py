from distutils.core import setup
from Cython.Build import cythonize
import os, sys
setup(
        ext_modules = cythonize('LinkedList.pyx')
        )

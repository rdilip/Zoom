import cPickle, random, cProfile
#from simanneal2 import Annealer
#from simanneal3 import Annealer
#from simanneal4 import Annealer

#from simanneal3Py import Annealer
#from simannealLazy import Annealer
#from simanneal import Annealer
import time
from simanneal4Py import Annealer
import sys
import time
def test():
    global elapsed
    sz = 50
    with open('QUBO_B_6-12_N_50_09.qubo', 'r') as f:
        Q = cPickle.load(f)
#    Q = {(0,1): -1, (1,5): -1, (1,4):-1, (2,4):-1, (1, 3):-1}
    state = [random.choice([0, 1]) for i in xrange(sz)]
#    A = Annealer(state, Q)
    start = time.time()
    A = Annealer(state, Q, sz)
    elapsed = time.time() - start
    return A

B = test()

cmd = 'print B.simulate()'
cProfile.run(cmd)
print "Initialization time: {0}".format(elapsed)


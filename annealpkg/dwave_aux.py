"""
.. module:: useful_1
   :platform: Unix, Windows
   :synopsis: A module to simplify analysis on the DWave machine.

.. moduleauthor:: Rohit Dilip <rdilip@princeton.edu>


"""
from copy import copy as cpy
import os, cPickle
import cPickle
from random import *
import networkx as nx
from dwave_sapi2.util import get_chimera_adjacency, qubo_to_ising
from dwave_sapi2.embedding import find_embedding, embed_problem
from collections import defaultdict
from dwave_sapi2.embedding import find_embedding
import matplotlib.pyplot as plt
from glob import glob

# IO FUNCTIONS
def load_file(base,size,inst,filetype='qubo'):
    """Load a pickled file from the given parameters. 
    Args:
        base (str): Numerical pattern of file. Must be string of integers separated by hyphens 
        size (int): Size of the specific file to be loaded
        inst (int): Instance ID of file to be loaded

    Kwargs:
        filetype (str): ('qubo', 'equbo', 'vqubo', 'embe', 'hfield')

    Returns:
        dict. The return file.

    Raises:
       AttributeError, KeyError

    Usage:
        >>> A = load_file('4-8', 40, 32, filetype='vqubo')

    """
    ftype = filetype.lower()
    flocation = 'qubo_files/{0}/N_{1}/{3}/{3}_B_{0}_N_{1}_{2:02d}.{4}'.format(base,size,inst,ftype.upper(),ftype)
    with open(flocation, 'r') as f:
        return cPickle.load(f)


def die(message, e):
    """ Kills program. Writes error type to error_reports.txt 
    Args:
        message (str): Message to return after killing program
        e (Exception): Exception type

    Returns:
        void.
    """
    print message
    with open('error_reports.txt', 'a+') as f:
        f.write('{0}\n{1}\n'.format(message, str(e)))
    raise e

def write_file(f, base, size, inst, filetype='qubo'):
    """ Write an object in pickled form to the appropriate location
    Args:
        base (str): Numerical pattern of file. Must be string of integers separated by hyphens 
        size (int): Size of the specific file to be loaded
        inst (int): Instance ID of file to be loaded

    Kwargs:
        filetype (str): ('qubo', 'equbo', 'vqubo', 'embe', 'hfield')

    Returns:
        dict. The return file.

    Usage:
        >>> write_file([jc, j0], '4-8', 40, 32, filetype='vqubo')

        Note that if the file is an equbo or vqubo, it should be in list form [jc, j0], where jc
        is a dict representing internal couplings, and j0 is a dict representing external couplings.


    Raises:
       TypeError 
    """
    if filetype == 'qubo' and type(f) != list:
        die("Expected list for qubo, received {0}".format(type(f)), TypeError)
    elif (filetype == 'equbo' or filetype == 'vqubo' and (type(f) != list or len(f) != 2 or type(f[0]) != dict or type(f[1]) != dict)):
        die("Expected list for (v/e)qubo, of form [jc, j0]. Received {0}".format(type(f)), TypeError)
    elif (filetype == 'embe' and type(f) != list):
        die("Expected list for embe, received {0}".format(type(f)), TypeError)
    elif (filetype == 'hfield' and type(f) != list):
        die("Expected list for hfield, received {0}".format(type(f)), TypeError)

    ftype = filetype.lower()
    flocation = 'qubo_files/{0}/N_{1}/{3}/{3}_B_{0}_N_{1}_{2:02d}.{4}'.format(base,size,inst,ftype.upper(),ftype)

    with open(flocation, 'w+') as f1:
        try:
            cPickle.dump(f1, flocation, 2)
        except Exception as e:
            die("Could not write file", e)


def setup_folders_rohit_helper(BASES, SIZES, TIMES):
    """ Generates the proper directory structure for qubo files 
    Args:
        BASES (list): List of all bases. Each base should be in string format.
        SIZES (list): List of all sizes. Each size should be an integer.
        TIMES (list): List of all times. Each time should be an integer.
    Returns:
        void
    Usage:
        Run this with the appropriate lists as input before writing any files.
    """
    paths = []
    for base in BASES:
        for size in SIZES:
            paths.extend(['qubo_files/%s' % base, \
                'qubo_files/%s/N_%d' % (base, size), \
                'qubo_files/%s/N_%d/QUBO' % (base, size), \
                'qubo_files/%s/N_%d/HFIELD' % (base, size), \
                'qubo_files/%s/N_%d/EQUBO' % (base, size), \
                'qubo_files/%s/N_%d/EMBE' % (base, size), \
                'qubo_files/%s/N_%d/VC-QUBO' % (base, size), \
                'qubo_files/%s/N_%d/RESULTS' % (base, size)])
    for dir_ in paths:
        if not os.path.exists(dir_):
            os.mkdir(dir_)
    for time in TIMES:
        path = 'qubo_files/%s/N_%d/RESULTS/%d' % (base, size, time)
        if not os.path.exists(path):
            os.mkdir(path)

        # HARD CALCULATIONS

def ferro_upper_bound(embedding, j0, h_fields, p):
    """ Returns an upper bound (determined by Vicky Choi's paper,
    section 4.1) on the ferromagnetic coupler strength to ensure
    qubits remain in alignment. 
    Args:
        embedding (list): Embedding of problem onto adjacency matrix
        j0 (dict): External couplings (NOT internal of logical qubits
        h_fields (list): List of local fields
        p (int): Index of logical qubit

    Returns:
        int. Upper bound determined by VC paper.
    Usage:
        This function sums the external couplings, weighted by the coupler strengths, 
        for each physical qubit, then selects the highest strength and adds the
        partial h_field strength to this value to provide an upper bound.
    Raises:
        IndexError
        """

    F_pq=0
    max_F_pq=0
    M=float(len(embedding[p]))
    for i in embedding[p]:
        F_pq=0
        for j in j0.keys():
            if i in j:
                F_pq += abs(j0[j])

        if F_pq > max_F_pq: max_F_pq=F_pq
    if len(h_fields) != len(embedding):
        h_fields.extend([0] * (len(embedding) - len(h_fields)))
    try: 
        max_F_pq += abs(h_fields[p] / M)
    except IndexError as e:
        die("Index error in finding maximum ferromagnetic bound", e)

    return -1.0 * max_F_pq

def find_logical_qubit(con, embeddings):
    i, j=con
    for k in range(len(embeddings)):
        if i in embeddings[k] or j in embeddings[k]: return k

def chunkify(lst,n):
    return [lst[i::n] for i in xrange(n)]


def max_conns_list(num_nodes, base='4-8'):
    """ Returns a degree sequence based on the base. 
    Args:
        num_nodes (int): Size of degree sequence
    Kwargs:
        base (str): numerical pattern of degree sequence. Must be a string
        composed of integers seperated by hyphens
    Returns:
        list. A shuffled degree sequence
    Usage:
        >>> print max_conns_list(20, base='4-8')    
        [8, 8, 4, 4, 8, 8, 4, 4, 4, 4, 8, 8, 4, 8, 4, 4, 8, 8, 8, 4]
        
    """
    vals = [int(i) for i in base.split('-')]
    A = [0] * num_nodes
    num_bases = len(vals)
    A = chunkify(A, len(vals))
    for lst in range(num_bases):
        for i in range(len(A[lst])):
            A[lst][i] = vals[lst]
    A = [item for sublist in A for item in sublist]
    shuffle(A)
    return A


def connectivity(p, conns):
    """ Returns connectivity of a given node
    Args:
        p (int): Index of node
        conns (list): Edgelist. Each edge is represented as a tuple
    Returns:
        int. Connectivity of a node
    Usage:
        Connections list is NOT symmetric -- each tuple must have the lesser
        value come first
    """
    count=0
    for i in conns:
        if p in i:
            count += 1
    return count


def generate_random_qubo(size, base='4-8'):
    """ Generates a random QUBO based on a given size and base.
    Args:
        size (int): Size of QUBO

    Kwargs:
        base (str): Numerical pattern on which to base QUBO
    
    Returns:
        dict. Randomly generated QUBO

    Usage:
        Performs a Monte-Carlo random sampling method that preserves
        connectivity but ensures uniform sampling from the ensemble
        of graphs.
    """
    deg=max_conns_list(size, base)
    G=nx.random_degree_sequence_graph(deg)
    E=list(set(G.edges()))
    count = 0
    while count < size: count += monte_carlo_switch(E)
    Jf=[choice([-1,1]) for i in range(len(E))]
    return dict(zip(E, Jf))

    
def monte_carlo_switch(E, max_size=0):
    """ For a given edgelist, randomly swaps edges while preserving
    connectivity to ensure uniform sampling from the total ensemble
    of graphs. 
    Args:
        E (list): Edgelist
    Kwargs:
        max_size: Maximum size of graph corresponding to E
    Usage:
        >>> E = [(0, 2), (0, 4), (0, 5), (1, 4), (1, 9), (2, 10), (3, 7), (3, 8), (5, 7), (6, 8), (6, 10), (6, 11), (7, 9), (8, 11), (10, 11)
        >>> monte_carlo_switch(E)
        >>> print sorted(E)
        [(0, 2), (0, 4), (0, 5), (1, 6), (1, 9), (2, 10), (3, 7), (3, 8), (4, 8), (5, 7), (6, 10), (6, 11), (7, 9), (8, 11), (10, 11)]
        """
    edges = random_edges(E)
    A1 = [connectivity(i, E) for i in range(max_size)]
    (x, y) = edges[0]
    (u, v) = edges[1]
    edge1 = (min(x, u), max(x, u))
    edge2 = (min(y, v), max(y, v))
    if (edge1 not in E) and (edge2 not in E):
        E.remove(edges[0])
        E.remove(edges[1])
        E.extend([edge1, edge2])
        A2 = [connectivity(i, E) for i in range(max_size)]
        if A1 != A2:
            print edge1, edge2
            print "PROBLEM"
        return 1
    return 0


def random_edges(E):
    (a, b) = choice(E)
    ACCEPT = False
    while not ACCEPT:
        edge2 = choice(E)
        if a not in edge2 and b not in edge2:
            ACCEPT = True
    return [(a, b), edge2]


def avg_connectivity(num_nodes, E):
    """ Computes the average connectivity of an edgelist
    Args:
        num_nodes (int): Number of nodes 
        E (list): Edgelist

    Returns:
        float. Average connectivity
    """
    count=0.0
    for i in xrange(num_nodes):
        count += connectivity(i, A)
    return count / float(num_nodes)



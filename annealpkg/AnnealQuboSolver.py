from dwave_aux import load_file
from random import choice, shuffle
from simanneal4 import Annealer

global BASES, SIZES, INSTANCES

def main():
    BASES = ['6','9','12','4-8','6-12','8-16','4-6-8','6-9-12','8-12-16']
    SIZES = range(20, 70, 10)
    INSTANCES = range(100)
    for base in BASES:
        for size in SIZES:
            print "Starting BASE {0}, SIZE {1}".format(base, size)
            T = []
            for inst in INSTANCES:
                try:
                    T.append(load_file(base, size, inst, 'qubo'))
                except: continue
            with open('qubo_files/{0}/N_{1}/CONF-E_B_{0}_N_{1}.txt'.format(base,size), 'w+') as f:
                inpState = [choice([0, 1]) for i in xrange(size)]
                for q in T:
                    a = ""
                    try:
                        A = Annealer(inpState, q, size)
                        a = A.simulate()
                    except:
                        print "ERROR"
                        exit(0)
                    f.write(str(a) + '\n')

if __name__ == '__main__':
    main()

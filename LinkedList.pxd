import copy
cdef class Node:
    cdef int val
    cdef Node nx
    cdef set_next(self, new_next)
    cpdef value(self)
    cpdef get_next(self)


cdef class LinkedList:
    cdef Node st
    cdef int N
    cpdef put(self, data)

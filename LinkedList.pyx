import copy
cdef class Node:
    def __cinit__(self, data=None, next_node=None):
        self.val = data
        self.nx = next_node

    cpdef value(self):
        return self.val

    cpdef get_next(self):
        return self.nx

    cdef set_next(self, new_next):
        self.nx = new_next

cdef class LinkedList:
    def __cinit__(self, head=None):
        self.st = head
        self.N = 0

    cpdef put(self, data):
        cdef Node new_node = Node(data)
        new_node.set_next(self.st)
        self.st = new_node
        self.N += 1

    def __iter__(self):
        cdef Node nd = self.st
        yield nd.value()
        while nd.get_next() != None:
            nd = nd.get_next()
            yield nd.value()
    
    def __str__(self):
        nd = self.st
        A = []
        A.append(nd.value())
        while nd.get_next() != None:
            A.append(nd.get_next().value())
            nd = nd.get_next()
        return ', '.join(map(str, A))

    def size(self):
        return self.N


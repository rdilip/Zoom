import cProfile
from random import choice
import numpy as np
from libc.math cimport exp, log
from libc.stdlib cimport rand, RAND_MAX
from libc.stdlib cimport malloc, free
cimport cython
import cPickle
from LinkedList import LinkedList
from collections import deque
################################################################################

cdef class Annealer:

    cdef:
        int *state
        conns
        adj
        float *dE_array
        float E
        float Tmin
        float Tmax
        int steps
        int updates 
        int sz
        
    def __cinit__(self, state, connections, int size):
        self.state = <int *> malloc(size * cython.sizeof(int))
        self.dE_array = <float *> malloc(size * cython.sizeof(float))
        if self.state is NULL:
            raise MemoryError()
        if self.dE_array is NULL:
            raise MemoryError()

        for i in xrange(size):
            self.state[i] = state[i]

        self.conns = connections
        self.E = self.energy_init()
        self.Tmin = 0.01
        self.Tmax = 100
        self.steps = 50000
        self.updates = 100
        self.sz = size
        self.adj = []
        #for i in xrange(size):
        #    self.adj.append(deque())
        #for i in connections:
        #    self.adj[i[0]].append(i[1])
        #    self.adj[i[1]].append(i[0])
        for i in xrange(size):
            self.adj.append(LinkedList())

        for i in connections:
            self.adj[i[0]].put(i[1])
            self.adj[i[1]].put(i[0])
        for i in xrange(size):
            self.dE_array[i] = (-self.delta_energy(i))

    def set_schedule(self, Tmin, Tmax, steps):
        self.Tmin = Tmin
        self.Tmax = Tmax
        self.steps = 50000

    cdef float delta_energy(self, int i):
        cdef: 
            float energy = 0.0
        for j in self.adj[i]:
            tmp = (i, j)
            if i > j: tmp = (j, i)
            energy += self.conns[tmp] * self.state[j]
        energy *= (2 * self.state[i]) - 1
        return energy

    cdef float energy_init(self):
        energy = 0.0
        for conn in self.conns:
            if conn[0] == conn[1]:
                energy += self.conns[conn] * self.state[conn[0]]
                continue
            else:
                energy += self.conns[conn] * self.state[conn[0]] * \
                        self.state[conn[1]]
        return energy

    cdef int move(self):
       cdef int i = rand() % self.sz
       if self.state[i] == 0:
           self.state[i] = 1
       else:
           self.state[i] = 0
       return i

    cdef void update(self, float T, float accept):
        print("Energy: {0}\tTemp: {1:.3f}\tAccept: {2:.2f}%".format(self.E,\
                T, accept * 100))

    cpdef simulate(self, toUpdate = False):
        cdef:
            float Tchange = -log(self.Tmax / self.Tmin)
            int interval = self.steps / self.updates
            float step = 0.0
            float trials = 0.0
            float accept = 0.0
            float T
            float E_old
            float dE
            float prob
            int i
            int *oldState
        global oldState
        if toUpdate: 
            self.update(self.Tmax, 1.0)
        while step < self.steps:
            step += 1
            trials += 1

            oldState = <int *> malloc(self.sz * cython.sizeof(int))

            for i in xrange(self.sz):
                oldState[i] = self.state[i]


            T = self.Tmax * exp(Tchange * step / self.steps)

            i = self.move()
            dE = self.dE_array[i]
            prob = float(rand()) / RAND_MAX

            if (dE > 0.0 and prob > exp(-dE / T)):
                self.state = oldState
            else:
                accept += 1
                for j in self.adj[i]:
                    tmp = (i, j)
                    if (i > j): tmp = (j, i)
                    self.dE_array[j] -= (2 * self.state[i] - 1) * (2 * self.state[j] - 1) \
                            * self.conns[tmp]
                self.dE_array[i] *= -1
                self.E += dE

            if (toUpdate):  
                if (step % interval == 0):
                    self.update(T, accept / trials)
                    trials = 0.0
                    accept = 0.0
        tmp = []
        for i in xrange(self.sz):
            tmp.append(self.state[i])
        self.E = self.energy_init()
        return (tmp, self.E)



#if __name__ == '__main__':
#    with open('QUBO_B_6-12_N_50_09.qubo', 'r') as f:
#        Q = cPickle.load(f)
#    state = [random.choice([0, 1]) for i in xrange(50)]
#    A = Annealer(state, Q)
    # UNIT TEST
    #print A.state
    #tmp = copy.deepcopy(A.state)
    #print A.E

    #i = A.move()
    #print "We flipped {0}".format(i)

    #print "Change in energy: {0}".format(A.delta_energy(i))
    #print A.state
    #print A.E
    #print "Is the old state the same as the new state?: {0}".format(A.state == tmp)
    #cProfile.run('A.simulate()')


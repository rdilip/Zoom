import copy
class Node(object):

    def __init__(self, data=None, next_node=None):
        self.data = data
        self.next_node = next_node

    def value(self):
        return self.data

    def get_next(self):
        return self.next_node

    def set_next(self, new_next):
        self.next_node = new_next

class LinkedList(object):
    
    def __init__(self, head=None):
        self.head = head
        self.N= 0

    def put(self, data):
        new_node = Node(data)
        new_node.set_next(self.head)
        self.head = new_node
        self.N += 1

    def __iter__(self):
        nd = self.head
        yield nd.value()
        while nd.get_next() != None:
            nd = nd.get_next()
            yield nd.value()
    
    def __str__(self):
        nd = self.head
        A = []
        A.append(nd.value())
        while nd.get_next() != None:
            A.append(nd.get_next().value())
            nd = nd.get_next()
        return ', '.join(map(str, A))

    def size(self):
        return self.N


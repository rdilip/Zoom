import cProfile
import numpy as np
from BinarySearch import allMatches
from libc.math cimport exp, log
from libc.stdlib cimport rand, RAND_MAX

#cdef extern from "math.h":
#    double exp(double x)
#    double log(double x)

################################################################################

cdef class Annealer:
    cdef:
        state
        float E
        float Tmin
        float Tmax
        int steps
        int updates
        conns
        connsFirst
        connsSec

    def __cinit__(self, state, connections):
        self.state = state
        self.conns = connections
        self.connsFirst = sorted(connections.keys())
        self.connsSec = sorted(connections.keys(), key = lambda x: x[1])
        self.E = self.energy_init()
        self.Tmin = 0.01
        self.Tmax = 50
        self.steps = 50000
        self.updates = 10

    def set_schedule(self, Tmin, Tmax, steps):
        self.Tmin = Tmin
        self.Tmax = Tmax
        self.steps = 50000

    def set_updates(self, updates):
        self.updates = updates

    cdef delta_energy(self, int i):
        cdef float energy = 0.0
        for j in allMatches(self.connsFirst, i, 0):
            energy += self.conns[j] * self.state[j[1]]
        for j in allMatches(self.connsSec, i, 1):
            energy += self.conns[j] * self.state[j[0]]
        energy *= (2 * self.state[i]) - 1
        return energy

    cdef energy_init(self):
        cdef float energy = 0.0
        for conn in self.conns:
            if conn[0] == conn[1]:
                energy += self.conns[conn] * self.state[conn[0]]
                continue
            else:
                energy += self.conns[conn] * self.state[conn[0]] * \
                        self.state[conn[1]]
        return energy

    cdef move(self):
        cdef int i = (int)(float(rand()) / RAND_MAX * len(self.state))
        if self.state[i] == 0:
            self.state[i] = 1
        else:
            self.state[i] = 0
        cdef float dE = self.delta_energy(i)
        self.E += dE
        return i

    cdef update(self, T, accept):
        print("Energy: {0}\tTemp: {1:.3f}\tAccept: {2:.2f}%".format(self.E,\
                T, accept * 100))

    cpdef simulate(self, toUpdate = False):
        if toUpdate: self.update(self.Tmax, 1.0)
        cdef: 
            float Tchange = -log(self.Tmax / self.Tmin)
            int interval = self.steps / self.updates
            step = 0.0
            trials = 0.0
            accept = 0.0
            tmp = 0
        while step < self.steps:
            step += 1
            trials += 1

            T = self.Tmax * exp(Tchange * step / self.steps)
            oldState = list(self.state)
            E_old = self.E
            self.move()
            dE = self.E - E_old
            if (dE > 0.0 and (float(rand()) / RAND_MAX) > exp(-dE / T)):
                self.state = oldState
                self.E = E_old
            else:
                tmp += dE
                accept += 1

        if (toUpdate and step % interval == 0):
            self.update(T, accept / trials)
            trials = 0.0
            accept = 0.0
        print self.E
        return self.state


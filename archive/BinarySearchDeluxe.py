import random
import numpy as np
from copy import deepcopy
from bisect import bisect_left, bisect_right, bisect
import math

def firstIndexOf(a, key, i = 0):
    lo = 0
    mid = len(a) / 2
    hi = len(a) - 1
    res = -1
    while (lo <= hi):
        mid = (hi + lo) / 2
        if (comp(key, a[mid], i) == 0):
            res = mid
            hi = mid - 1
        elif    comp(key, a[mid],  i) == +1: lo = mid + 1
        else: hi = mid - 1
    return res

def lastIndexOf(a, key, i = 0):
    lo = 0
    mid = len(a) / 2
    hi = len(a) - 1
    res = -1
    while (lo <= hi):
        mid = (lo + hi) / 2
        if (comp(key, a[mid], i) == 0):
            res = mid
            lo = mid + 1
        elif comp(key, a[mid], i) == -1: hi = mid - 1
        else: lo = mid + 1
    return res

def allMatches(a, val, i = 0):
    key = (val, 0)
    if i == 1: key = (0, val)
    return a[firstIndexOf(a, key, i) : lastIndexOf(a, key, i) + 1]

def comp(tup, key, i = 0):
    if tup[i] == key[i]:        return 0
    elif tup[i] < key[i]: return -1
    else:                 return +1

def main():
    p = [(random.choice(range(3)), random.choice(range(3))) for i in xrange(10)]
    p.extend([(1,1), (1,1)])
    p = sorted(p)
    q = sorted(deepcopy(p), key = lambda x: x[1])
    p_ = [i[0] for i in p]
    q_ = [i[1] for i in q]
    print p
    print q
    print p[bisect_left(p_, 2): bisect_right(p_, 2)]
    print q[bisect_left(q_, 2): bisect_right(q_, 2)]

    #print p
    #print allMatches(p, 2, 0)
    #print allMatches(q, 2, 1)
    #p = [(1,1)] * 8
    #print p
    #print firstIndexOf(p, (1, 1)), lastIndexOf(p, (1, 1))
    #print allMatches(p, (1,1), 0)


if __name__ == '__main__':
    main()

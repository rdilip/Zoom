import random 
import cProfile
import copy
import math
import numpy as np
import cPickle
from BinarySearch import allMatches
################################################################################

class Annealer:
    def __init__(self, state, connections):
        self.state = (state)
        self.conns = connections
        self.connsFirst = sorted(connections.keys())
        self.connsSec = sorted(connections.keys(), key = lambda x: x[1])
        self.E = self.energy_init()
        self.Tmin = 0.01
        self.Tmax = 50
        self.steps = 50000
        self.updates = 10

    def set_schedule(self, Tmin, Tmax, steps):
        self.Tmin = Tmin
        self.Tmax = Tmax
        self.steps = 50000

    def delta_energy(self, i):
        energy = 0
        for j in allMatches(self.connsFirst, i, 0):
            energy += self.conns[j] * self.state[j[1]]
        for j in allMatches(self.connsSec, i, 1):
            energy += self.conns[j] * self.state[j[0]]
        energy *= (2 * self.state[i]) - 1
        return energy

    def energy_init(self):
        energy = 0.0
        for conn in self.conns:
            if conn[0] == conn[1]:
                energy += self.conns[conn] * self.state[conn[0]]
                continue
            else:
                energy += self.conns[conn] * self.state[conn[0]] * \
                        self.state[conn[1]]
        return energy

    def move(self):
       i = random.randrange(len(self.state))
       if self.state[i] == 0:
           self.state[i] = 1
       else:
           self.state[i] = 0
       dE = self.delta_energy(i)
       self.E += dE
       return i

    def update(self, T, accept):
        print("Energy: {0}\tTemp: {1:.3f}\tAccept: {2:.2f}%".format(self.E,\
                T, accept * 100))

    def simulate(self):
        #self.update(self.Tmax, 1.0)
        Tchange = -math.log(self.Tmax / self.Tmin)
        interval = self.steps / self.updates
        step = 0.0
        trials = 0.0
        accept = 0.0
        tmp = 0
        while step < self.steps:
            step += 1
            trials += 1

            T = self.Tmax * math.exp(Tchange * step / self.steps)
            oldState = list(self.state)
            E_old = self.E
            self.move()
            dE = self.E - E_old
            if (dE > 0.0 and random.random() > math.exp(-dE / T)):
                self.state = oldState
                self.E = E_old
            else:
                tmp += dE
                accept += 1

#            if (step % interval == 0):
#                self.update(T, accept / trials)
#                trials = 0.0
#                accept = 0.0
        print self.state
        print self.E


if __name__ == '__main__':
    with open('QUBO_B_6-12_N_50_09.qubo', 'r') as f:
        Q = cPickle.load(f)
    state = [random.choice([0, 1]) for i in xrange(50)]
    A = Annealer(state, Q)
    # UNIT TEST
    #print A.state
    #tmp = copy.deepcopy(A.state)
    #print A.E

    #i = A.move()
    #print "We flipped {0}".format(i)

    #print "Change in energy: {0}".format(A.delta_energy(i))
    #print A.state
    #print A.E
    #print "Is the old state the same as the new state?: {0}".format(A.state == tmp)
    cProfile.run('A.simulate()')


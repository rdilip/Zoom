from bisect import bisect_left, bisect_right
cdef firstIndexOf(a, key, i = 0):
    cdef: 
        int lo = 0
        int mid = len(a) / 2
        int hi = len(a) - 1
        int res = -1
    while (lo <= hi):
        mid = (hi + lo) / 2
        if (comp(key, a[mid], i) == 0):
            res = mid
            hi = mid - 1
        elif    comp(key, a[mid],  i) == +1: lo = mid + 1
        else: hi = mid - 1
    return res

cdef lastIndexOf(a, key, i = 0):
    cdef:
        int lo = 0
        int mid = len(a) / 2
        int hi = len(a) - 1
        int res = -1
    while (lo <= hi):
        mid = (lo + hi) / 2
        if (comp(key, a[mid], i) == 0):
            res = mid
            lo = mid + 1
        elif comp(key, a[mid], i) == -1: hi = mid - 1
        else: lo = mid + 1
    return res

def allMatches(a, val, i = 0):
    key = (val, 0)
    if i == 1: key = (0, val)
    return a[firstIndexOf(a, key, i) : lastIndexOf(a, key, i) + 1]

cdef comp(tup, key, int i = 0):
    if tup[i] == key[i]:        return 0
    elif tup[i] < key[i]: return -1
    else:                 return +1


